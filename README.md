# woocomerce-splittypay-plugin-archive

Questo repository contiene l'archivio di tutte le versioni del plugin WooCommerce di SplittyPay. E' presente un branch per ogni versione del plugin, mentre all'interno del branch _master_ è pubblicata sempre l'ultima versione disponibile. 


## Come aggiornare il plugin ad una nuova versione

Upgrade all'ultima versione disponibile

- Naviga alla pagina https://bitbucket.org/splittypay/woocomerce-plugin-archive/src/master/
    - Clicca sulla sezione **_Downloads_** nel menù di sinistra
    - Clicca sulla voce "Download repository"
    - Decomprimi l'archivio e sposta in una directory a te comoda il file _woocommerce-gateway-splittypay-checkout.zip_
- Accedi al tuo sito WordPress ed esegui backup del sito
- Naviga nella sezione *Plugin -> Plugin installati*
- Disattiva il plugin **WooCommerce Splitty Pay Checkout Gateway**
- Cancella il plugin **WooCommerce Splitty Pay Checkout Gateway**
- Clicca il bottone in cima alla pagina "Aggiungi Nuovo"
- Clicca il bottone "Carica Plugin"
- Clicca "Scegli file" e seleziona la nuova versione del file _woocommerce-gateway-splittypay-checkout.zip_ precedentemente scaricato
- Attiva il plugin
- Verifica che le configurazioni del plugin non siano state cancellate/modificate

## RollBack ad una versione precedente

- Naviga alla pagina https://bitbucket.org/splittypay/woocomerce-plugin-archive/src/master/
    - Clicca sulla sezione **_Downloads_** nel menù di sinistra
    - Cambia la scheda cliccando _**Branches**_ 
    - Seleziona la versione che vuoi scaricare e scarica il repository cliccando sulla voce "zip"
    - Decomprimi l'archivio e sposta in una directory a te comoda il file _woocommerce-gateway-splittypay-checkout.zip_
- Accedi al tuo sito WordPress ed esegui backup del sito
- Naviga nella sezione *Plugin -> Plugin installati*
- Disattiva il plugin **WooCommerce Splitty Pay Checkout Gateway**
- Cancella il plugin **WooCommerce Splitty Pay Checkout Gateway**
- Clicca il bottone in cima alla pagina "Aggiungi Nuovo"
- Clicca il bottone "Carica Plugin"
- Clicca "Scegli file" e seleziona la versione del file _woocommerce-gateway-splittypay-checkout.zip_ precedentemente scaricato
- Attivare il plugin
- Verifica che le configurazioni del plugin non siano state cancellate/modificate